"use strict";


Vue.component('page-viewer', {
	props: {
		images: Array,
		doublePage: {
			type: Boolean,
			default: false,
		},
		fit: {
			type: String,
			default: 'fit-container',
		},
		ltrDirection: {
			type: Boolean,
			default: true,
		},
		posOffsetX: {
			type: String,
			default: '0px',
		},
		posOffsetY: {
			type: String,
			default: '0px',
		},
		sizeOffsetX: {
			type: String,
			default: '0px',
		},
		sizeOffsetY: {
			type: String,
			default: '0px',
		},
		currentPage: Number,
		preload: {
			type: Number,
			default: 5,
		},
	},
	data: function () {
		return {
			localImages: [],
			loading: false,
		};
	},
	watch: {
		images: function () {
			this.$nextTick(this.loadManager);
		},
		currentPage: function () {
			this.loadManager();
		},
	},
	computed: {
		containerUsualHeight: function () {
			return `calc(100vh - ${this.sizeOffsetY})`;
		},
		containerUsualWidth: function () {
			return `calc(100vw - ${this.sizeOffsetX})`;
		},
		containerUsualWidthStyle: function () {
			return {
				width: this.containerUsualWidth,
			};
		},
		containerStyles: function () {;
			return {
				'no-resize': {
					minHeight: this.containerUsualHeight,
					minWidth: this.containerUsualWidth,
				},
				'fit-height': {
					minHeight: this.containerUsualHeight,
					minWidth: this.containerUsualWidth,
				},
				'fit-width': {
					minHeight: this.containerUsualHeight,
					width : this.containerUsualWidth,
				},
				'fit-container': {
					height: this.containerUsualHeight,
					width: this.containerUsualWidth,
				},
			};
		},
		containerStyle: function () {
			return this.containerStyles[this.fit];
		},
		imgStyles: function () {
			return {
				'no-resize': {},
				'fit-height': {
					height: this.containerUsualHeight,
				},
				'fit-width': {
					width: this.containerUsualWidth,
					flexShrink: '1',
				},
				'fit-container': {
					maxHeight: this.containerUsualHeight,
					flexShrink: '1',
				},
			};
		},
		imgStyle: function () {
			return this.imgStyles[this.fit];
		},
		directionStyle: function () {
			return {flexDirection: this.ltrDirection ? 'row-reverse' : 'row'};
		},
		posOffsetStyle: function () {
			return {
				marginLeft: this.posOffsetX,
				marginTop: this.posOffsetY,
			};
		},
	},
	methods: {
		loadManager: function (event) {
			if (this.loading) {
				return;
			}
			let imgs = this.$refs.imgs;
			let span = Math.min(this.currentPage + this.preload, imgs.length);
			for (let i = this.currentPage; i < span; i += 1) {
				if (imgs[i].src) {
					continue;
				}
				imgs[i].src = this.images[i];
				if (imgs[i].complete) {
					continue;
				}
				this.loading = true;
				imgs[i].addEventListener('load', this.completeLoad);
				break;
			}
		},
		completeLoad: function (event) {
			this.loading = false;
			this.loadManager();
			event.target.removeEventListener('load', this.completeLoad);
		},
	},
	template: `
		<div
		 v-bind:style="containerUsualWidthStyle"
		>
			<div
			 id="images-container"
			 v-bind:style="[
				containerStyle,
				directionStyle,
				posOffsetStyle,
			 ]"
			>
				<img
				 v-for="(image, index) in images"
				 v-bind:key="index + image"
				 ref="imgs"
				 v-show="doublePage ? index == currentPage || index == currentPage + 1 : index == currentPage"
				 v-bind:style="imgStyle"
				 class="image"
				>
			</div>
			<slot>
			</slot>
		</div>
	`,
});


// Container for border-panel component instances

Vue.component('border-panels', {
	data: function () {
		return {
			borderPanels: [],
		};
	},
	methods: {
		reArrangePanels: function () {
			this.borderPanels.forEach((panel) => panel.computeAppearance());
		},
	},
	template: `
		<div>
			<slot>
			</slot>
		</div>
	`,
});


// border-panel component instances must be direct children of a
// border-panels component instance

Vue.component('border-panel', {
	props: {
		position: {
			type: String,
			validator: function (value) {
				return ['top', 'bottom', 'left', 'right'].indexOf(value) !== -1;
			},
		},
		direction: {
			type: String,
			validator: function (value) {
				return ['vertical', 'horizontal'].indexOf(value) !== -1;
			},
		},
		size: String,
		togglerSize: {
			type: String,
			default: '2rem',
		},
		togglerType: {
			type: String,
			validator: function (value) {
				return [
					'button',
					'hover-button',
					'hover-panel',
					'none',
				].indexOf(value) !== -1;
			},
			default: 'button',
		},
	},
	data: function () {
		return {
			index: 0,
			integrationStyle: {},
			togglerIntegrationStyle: {},
			hidden: false,
			togglerInvisible: false,
		};
	},
	computed: {
		sizeStyle: function () {
			if (this.position == 'top' || this.position == 'bottom') {
				return {height: this.size};
			}
			return {width: this.size};
		},
		togglerSizeStyle: function () {
			if (this.position == 'top' || this.position == 'bottom') {
				return {height: this.togglerSize};
			}
			return {width: this.togglerSize};
		},
		ZStyle: function () {
			let totalPanels = this.$parent.borderPanels.length
			return {
				zIndex: this.hidden ?
				        String(totalPanels - this.index) :
				        String(totalPanels * 2 - this.index),
			};
		},
	},
	mounted: function () {
		let borderPanels = this.$parent.$data.borderPanels;
		if (borderPanels.some((e) => e.$props.position === this.position)) {
			console.error('Error: More than one border panel in position ' +
			              this.position);
			this.$destroy();
			return;
		}
		this.index = borderPanels.length;
		this.computeAppearance();
		borderPanels.push(this);
		let toggler = this.$el.firstChild;
		if (this.togglerType != 'none') {
			toggler.addEventListener('click', this.toggleVisibility);
		}
		if (this.togglerType == 'hover-button') {
			this.togglerInvisible = this.hidden;
			toggler.addEventListener('mouseenter', this.hoverButtonMouseIn);
			toggler.addEventListener('mouseleave', this.hoverButtonMouseOut);
		}
		else
		if (this.togglerType == 'hover-panel') {
			this.hidden = true;
			this.togglerInvisible = true;
			toggler.addEventListener('mouseenter', this.hoverPanelMouseIn);
		}
		else
		if (this.togglerType  == 'none') {
			this.togglerInvisible = true;
			toggler.remove();  // Prevents overlapping elements below
		}
	},
	beforeDestroy: function () {
		this.$el.remove();
	},
	watch: {
		hidden: function () {
			this.$parent.reArrangePanels();
		},
	},
	methods: {
		computeAppearance: function () {
			let offsetFrom;
			let negativeSizeAxis;
			let negativeSizePositions;
			if (this.position == 'top' || this.position == 'bottom') {
				offsetFrom = 'left';
				negativeSizeAxis = 'width';
				negativeSizePositions = ['left', 'right'];
			}
			else
			if (this.position == 'left' || this.position == 'right') {
				offsetFrom = 'top';
				negativeSizeAxis = 'height';
				negativeSizePositions = ['top', 'bottom'];
			}
			let offsetPositionTotal = [];
			let negativeSizeTotal = [];
			let panels = this.$parent.$data.borderPanels;
			let checkedPanels =
				this.hidden && (this.togglerType == 'hover-button' ||
				                this.togglerType == 'hover-panel') ?
				panels.length :
				this.index;
			for (let i = 0; i < checkedPanels; i += 1) {
				let panel = panels[i];
				if (panel.$props.togglerType == 'hover-button' && panel.hidden)
				{
					continue;
				}
				if (panel.$props.position == offsetFrom) {
					if (!panel.$data.hidden) {
						offsetPositionTotal.push(panel.$props.size);
					}
					if (!panel.$data.togglerInvisible) {
						offsetPositionTotal.push(panel.$props.togglerSize);
					}
				}
				if (negativeSizePositions.indexOf(panel.$props.position) !== -1)
				{
					if (!panel.$data.hidden) {
						negativeSizeTotal.push(panel.$props.size);
					}
					if (!panel.$data.togglerInvisible) {
						negativeSizeTotal.push(panel.$props.togglerSize);
					}
				}
			}
			offsetPositionTotal = offsetPositionTotal.join(' + ');
			negativeSizeTotal = negativeSizeTotal.join(' + ');
			let style = {};
			style[this.position] = this.hidden ?
				'-' + this.size :
				'0';
			style[offsetFrom] = offsetPositionTotal ?
				`calc(${offsetPositionTotal})` :
				'0';
			style[negativeSizeAxis] = negativeSizeTotal ?
				`calc(100% - (${negativeSizeTotal}))` :
				'100%';
			this.integrationStyle = style;
			let togglerStyle = {};
			togglerStyle[this.position] = this.hidden ?
				'0' :
				this.size;
			togglerStyle[offsetFrom] = style[offsetFrom];
			togglerStyle[negativeSizeAxis] = style[negativeSizeAxis];
			this.togglerIntegrationStyle = togglerStyle;
		},
		toggleVisibility: function () {
			this.hidden = !this.hidden;
		},
		hoverButtonMouseIn: function () {
			if (!this.hidden) {
				return;
			}
			this.togglerInvisible = false;
		},
		hoverButtonMouseOut: function () {
			if (!this.hidden) {
				return;
			}
			this.togglerInvisible = true;
		},
		hoverPanelMouseIn: function (event) {
			this.hidden = false;
			this.$el.addEventListener('mouseleave', this.hoverPanelMouseOut);
		},
		hoverPanelMouseOut: function () {
			this.hidden = true;
			this.$el.removeEventListener('mouseleave', this.hoverPanelMouseOut);
		},
	},
	template: `
		<div>
			<div
			 class="panel-toggler"
			 v-bind:class="[
				{invisible: togglerInvisible}
			 ]"
			 v-bind:style="[
				togglerSizeStyle,
				togglerIntegrationStyle,
				ZStyle,
			 ]"
			>
			</div>
			<div
			 class="panel"
			 v-bind:class="direction"
			 v-bind:style="[
				integrationStyle,
				sizeStyle,
				ZStyle,
			 ]"
			>
				<slot>
				</slot>
			</div>
		</div>
	`,
});


Vue.component('page-selector', {
	props: {
		currentPage: Number,
		numPages: Number,
		appElem: Object,
	},
	methods: {
		setPage: function (event) {
			let dstPage = Number(event.target.dataset.page);
			this.$emit('gotopage', dstPage);
		},
	},
	template: `
		<div
		 id="page-selector-container"
		 v-on:click="setPage"
		>
			<div
			 class="page-selector-page"
			 v-for="page in numPages"
			 v-bind:data-page="page - 1"
			>
			</div>
		</div>
	`,
});


let app = new Vue({
	el: '#app',
	data: {
		pages: [],
		currentPage: 0,
		pagesFileUrl: 'default.json',
		doublePage: false,
		fit: 'fit-container',
		ltrDirection: true,
		sox: '0px',
		soy: '0px',
		pox: '0px',
		poy: '0px',
	},
	computed: {
		currentImages: function () {
			return [
				this.pages[this.currentPage],
				this.pages[this.currentPage + 1]
			];
		},
	},
	methods: {
		getPages: function () {
			console.log('Getting pages');
			fetch(this.pagesFileUrl)
				.then((response) => response.json())
				.then((result) => {
					this.pages = result;
					this.currentPage = 0;
				})
				.catch((err) => console.log(err));
		},
		goToPage: function (event) {
			this.currentPage = event;
		},
		navigatePages: function (event) {
			switch (event.key) {
			case 'a':
				if (this.currentPage > 0) {
					this.currentPage -= 1;
				}
				return;
			case 's':
				if (this.currentPage < this.pages.length - 1) {
					this.currentPage += 1;
				}
				return;
			case 'd':
				this.doublePage = !this.doublePage;
				// NOTE: it's possible to be in currentPage -1 when in doublePage mode
				if (this.currentPage < 0) {
					this.currentPage = 0;
				}
				return;
			case 'z':
				if (this.doublePage && this.currentPage > 0) {
					this.currentPage -= 2;
				}
				return;
			case 'x':
				if (this.doublePage && this.currentPage < this.pages.length - 2)
				{
					this.currentPage += 2;
				}
				return;
			case 'r':
				this.ltrDirection = !this.ltrDirection;
				return;
			case '1':
				this.fit = 'no-resize';
				return;
			case '2':
				this.fit = 'fit-height';
				return;
			case '3':
				this.fit = 'fit-width';
				return;
			case '4':
				this.fit = 'fit-container';
				return;
			default:
				return;
			}
		},
		enableNavigation: function () {
			console.log('Enabling navigation');
			document.addEventListener('keyup', this.navigatePages);
		},
		disableNavigation: function () {
			console.log('Disabling navigation');
			document.removeEventListener('keyup', this.navigatePages);
		},
	},
	created: function () {
		this.enableNavigation();
	},
	destroyed: function () {
		this.disableNavigation();
	},
});
