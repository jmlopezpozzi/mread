// Minimal server

"use strict";


const http = require('http');
const fs = require('fs');


let cwd = process.argv[2] ? process.argv[2] : process.cwd();


let server = http.createServer(requestHandler).listen({
	host: 'localhost',
	port: 8000,
});


function requestHandler(req, res) {
	console.log(`${req.headers.host} => ${req.method} ${cwd + req.url}`);
	if (req.method != 'GET') {
		res.end();
	}
	let path = cwd + req.url;
	fs.promises.stat(path)
		.then((stat) => {
			if (!stat.isFile()) {
				throw new Error(req.url + ' is not a file.');
			}
			let fileStream = fs.createReadStream(path);
			fileStream.on('error', (err) => {
				console.error(err);
				res.end();
			});
			res.writeHead(200, {
				'Content-Type': getType(req.url),
			});
			fileStream.pipe(res);
		})
		.catch((err) => {
			res.writeHead(404);
			res.end();
			console.error(err);
		});
}


function getType(url) {
	let extension = url.slice(url.lastIndexOf('.') + 1);
	switch (extension) {
	case 'htm':
	case 'html':
		return 'text/html';
	case 'json':
		return 'application/json';
	case 'css':
		return 'text/css';
	case 'js':
		return 'text/javascript';
	case 'png':
		return 'image/png';
	case 'jpg':
	case 'jpeg':
		return 'image/jpeg';
	case 'gif':
		return 'image/gif';
	case 'webp':
		return 'image/webp';
	default:
		return 'text/plain';
	}
}
